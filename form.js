const $inputName = document.getElementById('input-name');
const $inputEmail = document.getElementById('input-email');
const $inputPhone = document.getElementById('input-phone');
const $form = document.querySelector('form');
//
const isWord = (value) => {
  const pattern = /^[A-Za-z\s'-\.]+$/;
  return pattern.test(value);
};

const isEmailValid = (value) => {
  const pattern =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(value);
};
const isNumberTel = (value) => {
  const pattern = /^[0-9]+$/;
  return pattern.test(value);
};
// error
const showError = (input) => {
  input.style.borderBottomColor = 'red';
};
const removeError = (input) => {
  input.style.borderBottomColor = 'rgb(187, 185, 185)';
};
/* ----------- Funciones de  validacion de campos de entrada ----------- */
// nombre
const checkUsername = () => {
  const name = $inputName.value.trim().toLowerCase();
  if (isWord(name)) {
    removeError($inputName);
    return true;
  }

  showError($inputName);
  return false;
};
// email
const checkEmail = () => {
  const email = $inputEmail.value.trim().toLowerCase();
  switch (true) {
    case !isEmailValid(email):
      showError($inputEmail);
      break;
    default:
      removeError($inputEmail);
      return true;
  }
  return false;
};
// telefono
const checkPhone = () => {
  const phone = $inputPhone.value.trim();
  if (!isNumberTel(phone)) {
    showError($inputPhone);
  } else {
    removeError($inputPhone);
    return true;
  }
  return false;
};

const formulario = () => {
  $form.addEventListener('submit', (event) => {
    event.preventDefault();
    if (checkUsername() && checkEmail() && checkPhone()) {
      alert('enviado');
      $form.reset();
    } else {
      alert('error');
    }
  });
};
export { formulario };
