const $days = document.getElementById('days');
const $hours = document.getElementById('hours');
const $minutes = document.getElementById('minutes');
const $seconds = document.getElementById('seconds');

const getRemainTime = (params) => {
  const now = new Date();
  // restar la fecha actual conla fecha dada
  // y lo divides entre mil para pasarlo a segundos
  const remainTime = (new Date(params) - now + 1000) / 1000;
  let remainSeconds = ('0' + Math.floor(remainTime % 60)).slice(-2);
  let remainMinutes = ('0' + Math.floor((remainTime / 60) % 60)).slice(-2);
  let remainHours = ('0' + Math.floor((remainTime / 3600) % 24)).slice(-2);
  let remainDays = Math.floor(remainTime / (3600 * 24));
  return {
    remainTime,
    remainSeconds,
    remainMinutes,
    remainHours,
    remainDays,
  };
};

const countdown = (deadLine) => {
  let timerUpdate = setInterval(() => {
    let tiempo = getRemainTime(deadLine);

    $days.textContent = tiempo.remainDays;
    $hours.textContent = tiempo.remainHours;
    $minutes.textContent = tiempo.remainMinutes;
    $seconds.textContent = tiempo.remainSeconds;

    if (tiempo.remainTime <= 1) {
      clearInterval(timerUpdate);
    }
  }, 1000);
};

export { countdown };

// 1h 60 min
// 1min = 60sec
